package org.jpereda.bt;

/**
 *
 * @author jpereda
 */
public class IosPlatform extends Platform {

    private IosBTService iosBTService;
    
    @Override
    public BTService getBTService() {
        if(iosBTService==null){
            iosBTService=new IosBTService();
        }
        return iosBTService;
    }
    
}
