package org.jpereda.bt;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.jpereda.bt.Platform.BTStatus;

/**
 *
 * @author jpereda
 */
public class IosBTService implements BTService {
    
    private final ObjectProperty<BTStatus> status = new SimpleObjectProperty<>(BTStatus.BT_UNKNOWN);
    
    @Override
    public ReadOnlyObjectProperty<BTStatus> statusProperty() {
        return status;
    }
    
    @Override
    public void enable(boolean askUser){
        
    }
    
    @Override
    public void disable(){
        
    }
}
