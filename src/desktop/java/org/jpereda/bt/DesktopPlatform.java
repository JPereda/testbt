package org.jpereda.bt;

/**
 *
 * @author jpereda
 */
public class DesktopPlatform extends Platform {
    
    private DesktopBTService desktopBTService;
    
    @Override
    public BTService getBTService() {
        if(desktopBTService==null){
            desktopBTService=new DesktopBTService();
        }
        return desktopBTService;
    }
    
}
