package org.jpereda.bt;

/**
 *
 * @author jpereda
 */
public class AndroidPlatform extends Platform {

    private AndroidBTService androidBTService;
    
    @Override
    public BTService getBTService() {
        if(androidBTService==null){
            androidBTService=new AndroidBTService();
        }
        return androidBTService;
    }
    
}
