package org.jpereda.bt;

import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafxports.android.FXActivity;
import org.jpereda.bt.Platform.BTStatus;

/**
 *
 * @author jpereda
 */
public class AndroidBTService implements BTService {

    private final ObjectProperty<BTStatus> status = new SimpleObjectProperty<>(BTStatus.BT_UNKNOWN);
    
    private final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private final static int REQUEST_ENABLE_BT = 1;
    
    private final BTListener receiver;
    
    public AndroidBTService() {
        if (bluetoothAdapter == null) {
            // Device does not support Bluetooth
            status.set(BTStatus.BT_NOT_SUPPORTED);
        } else if (!bluetoothAdapter.isEnabled()) {
            // Device supports Bluetooth, but it is disabled
            status.set(BTStatus.BT_NOT_ENABLED);
        } else {
            // Device supports Bluetooth, and it is enabled
            status.set(BTStatus.BT_ENABLED);
        }
        
        FXActivity.getInstance().getApplication().registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity actvt, Bundle bundle) {
            }

            @Override
            public void onActivityStarted(Activity actvt) {
            }

            @Override
            public void onActivityResumed(Activity actvt) {
            }

            @Override
            public void onActivityPaused(Activity actvt) {
            }

            @Override
            public void onActivityStopped(Activity actvt) {
                
            }

            @Override
            public void onActivitySaveInstanceState(Activity actvt, Bundle bundle) {
            }

            @Override
            public void onActivityDestroyed(Activity actvt) {
                System.out.println("Activity destroyed");
                try {
                    FXActivity.getInstance().unregisterReceiver(receiver);
                } catch(IllegalArgumentException e){
                    System.out.println("Receiver not registered");
                }
            }
           
        });
        
        receiver = new BTListener();
        
        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        FXActivity.getInstance().registerReceiver(receiver, filter);
    }
            
    @Override
    public ReadOnlyObjectProperty<BTStatus> statusProperty() {
        return receiver.statusProperty();
    }
    
    @Override
    public void enable(boolean askUser){
        if(bluetoothAdapter!=null && !bluetoothAdapter.isEnabled()){
            if(askUser){
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                FXActivity activity=FXActivity.getInstance();
                // A dialog will appear requesting user permission to enable Bluetooth
                activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else {
                /*
                WARNING: Not recommended
                */
                bluetoothAdapter.enable();
            }
        }
    }
    
    @Override
    public void disable(){
        if(bluetoothAdapter!=null && bluetoothAdapter.isEnabled()){
            bluetoothAdapter.disable();
        }
    }
    
}
