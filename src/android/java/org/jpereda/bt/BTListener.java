package org.jpereda.bt;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.jpereda.bt.Platform.BTStatus;
import javafx.application.Platform;

/**
 *
 * @author jpereda
 */
public class BTListener extends BroadcastReceiver {

    private final ObjectProperty<BTStatus> status = new SimpleObjectProperty<>(BTStatus.BT_UNKNOWN);
    
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                                                 BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    Platform.runLater(()->status.set(BTStatus.BT_NOT_ENABLED));
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    Platform.runLater(()->status.set(BTStatus.BT_TURNING_OFF));
                    break;
                case BluetoothAdapter.STATE_ON:
                    Platform.runLater(()->status.set(BTStatus.BT_ENABLED));
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    Platform.runLater(()->status.set(BTStatus.BT_TURNING_ON));
                    break;
            }
        }
    }
    
    public ObjectProperty<BTStatus> statusProperty() {
        return status;
    }
    
}
