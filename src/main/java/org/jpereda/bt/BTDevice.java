package org.jpereda.bt;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author jpereda
 */
public class BTDevice {

    public BTDevice(int id, String name, String address, String uuid) {
        this.id.set(id);
        this.name.set(name);
        this.address.set(address);
        this.uuid.set(uuid);
    }
    
    private final StringProperty name = new SimpleStringProperty();

    public String getName() {
        return name.get();
    }

    public void setName(String value) {
        name.set(value);
    }

    public StringProperty nameProperty() {
        return name;
    }
    private final IntegerProperty id = new SimpleIntegerProperty();

    public int getId() {
        return id.get();
    }

    public void setId(int value) {
        id.set(value);
    }

    public IntegerProperty idProperty() {
        return id;
    }
    private final StringProperty address = new SimpleStringProperty();

    public String getAddress() {
        return address.get();
    }

    public void setAddress(String value) {
        address.set(value);
    }

    public StringProperty addressProperty() {
        return address;
    }
    private final StringProperty uuid = new SimpleStringProperty();

    public String getUuid() {
        return uuid.get();
    }

    public void setUuid(String value) {
        uuid.set(value);
    }

    public StringProperty uuidProperty() {
        return uuid;
    }
    
}