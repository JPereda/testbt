package org.jpereda.bt;

/**
 *
 * @author jpereda
 */
public abstract class Platform {
    
    public enum BTStatus{
        BT_UNKNOWN, BT_NOT_SUPPORTED, BT_NOT_ENABLED, BT_ENABLED, BT_TURNING_ON, BT_TURNING_OFF
    }
    
    public abstract BTService getBTService();
    
}
