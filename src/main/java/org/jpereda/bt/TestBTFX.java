package org.jpereda.bt;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author jpereda
 */
public class TestBTFX extends Application {

    @Override
    public void start(Stage stage) {
        BTService btService = PlatformFactory.getPlatform().getBTService();
        
        final Label label = new Label("Hello Bluetooth!");
        final Label status = new Label("Status: "+btService.statusProperty().get());
        btService.statusProperty().addListener((obs,o,o1)->
            status.setText("Status: "+o1.toString()));
        final Label connect1 = new Label("Enable BT (asking)");
        connect1.setOnMouseClicked(e->btService.enable(true));
        final Label connect2 = new Label("Enable BT (without asking)");
        connect2.setOnMouseClicked(e->btService.enable(false));
        final Label disconnect = new Label("Disable BT");
        disconnect.setOnMouseClicked(e->btService.disable());
        
        VBox root = new VBox(20,label,status, connect1, connect2, disconnect);
        root.setAlignment(Pos.CENTER);
        
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        Scene scene = new Scene(root, visualBounds.getWidth(), visualBounds.getHeight());

        stage.setScene(scene);
        stage.show();
    }

}
