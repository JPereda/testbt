package org.jpereda.bt;

import javafx.beans.property.ReadOnlyObjectProperty;
import org.jpereda.bt.Platform.BTStatus;

/**
 *
 * @author jpereda
 */
public interface BTService {
    
    ReadOnlyObjectProperty<BTStatus> statusProperty();
    
    void enable(boolean askUser);
    
    void disable();
}
